# VigerousResearch

A revision resource for select subjects that I am currently studying. This includes:
- GCSE OCR Computer Science 
- GCSE AQA Product Design
- GCSE AQA Combined Science Trilogy

In this repository, I will include all assets used, and other links to external resources.

### Notice:

Some subjects and topics will rapidly change and update, so always download the most recent copy when possible.