# Computer Science
## GCSE - OCR
This document is being actively developed. If you find an error, please file a bug report, or help by contributing.
The latest version of this document is hosted [here](https://gitea.com/Snazzykitten/VigerousResearch/src/commit/1982d431a8e86317539e9e64aef008a545c07f25/README.md).


## Specification

| Paper | Topic 
| :---- | :----
| 1.1 | Systems Architecture
| 1.2 | Memory and Storage
| 1.3 | Computer Networking
| 1.4 | Network Security
| 1.5 | Systems Software
| 1.6 | Impacts of digital technology
| 2.1 | Algorithms

## Systems Architecture

### Cache Memory

Cache is a type of high-speed memory random-access memory (RAM) which is built into the CPU.

Data can be transfered to and from cache memory more quickly than from RAM. Due to this, cache memory is usually used to store frequently used instructions. This allows the processer to function faster as it does not have to wait for data to be fetched from RAM.

Generally, the more cache memory a computer has, the faster it runs. However, because of its high-speed performance, cache memory is more expensive to build than RAM. Therefore, cache memory tends to be very small in size.
To bypass this issue, different types of cache exist:
- L1 cache has extremely fast transfe rates, but is very small in size. The processor will use L1 cache to hold the most frequently used instructions and data.
- L2 cache is bigger in capacity than L1 cache, but slower in speed, It is used to hold data and instructions that are needed less frequently. 

### Von Neumann Architecture

Von Neumann Architecture is the design most modern, general purpose computers are based on. The Key elements of Von Neumann Architecture are:
- Data and instructions are both stored as binary digits
- Data and instructions are both stored in primary storage (stored program concept)
- Instructions are fetched from memory one at a time and in order (serially)
- The processor decodes and executes an instruction, before cycling around to fetch the next instruction
- The cycle continues until no more instructions are available
- Is has a single Control Unit (CU), onboard Cache, and an Internal Clock

A processor based on Von Neumann Architecture has five special registers which it uses for processing:
- The Program Counter (PC) holds the memory address of the next instruction to be fetched from primary storage
- The Memory Address Register (MAR) holds the address of the current instruction tht is to be fetched from memory, or the address in memory to which data is to be transferred.
- The memory data register (MDR) holds the contents found at the address held in the MAR, or data which is to be transferred to primary storage
- (beyond spec) - The current instruction register (CIR) holds the instruction that is currently being decoded and executed
- The accumulator (ACC) is a special purpose register and is used by the arithmetic logic unit (ALU) to hold the data being processed and the results of calculations
![img](https://bam.files.bbci.co.uk/bam/live/content/zb3yjhv/medium)

### Fetch-Execute

A computer can be defined as electronic device that takes an input, processes datam and deliver output.

A computer carries out its function by fetching instructions, decoding them, and executing them in a **repetitive** cycle billions of times a second. This is known as the **Fetch-Execute Cycle**.

The fetch stage invlove fetching the next intruction from main memory, and brings it back into the CPU
The decode stage involves inspecting the instruction to work out what needs doing.
The execute stage involves carrying out the instruction.

### CPU components

The CPU is made up of 4 main parts, the ALU, the CU, the Cache, and the registers.

#### Registers, and their function

| Register | Function
| :-------- | :-------
| Program Counter (PC) | Holds the address of the next instruction to be executed in memory.
| Memory Address Register (MAR) | Holds the address of where data is to be fetched or stored.
| Memory Data Register (MDR) | Holds the data fetched from, or to be written to memory.
| Accumulator | Holds the result of calculations

### Revision Resources

Theory:

[Craig'n'Dave](https://youtube.com/playlist?list=PLCiOXwirraUAEhj4TUjMxYm4593B2dUPF&si=cfLfN0Y9SL2yUgEW) Full theory course (FREE)

[MrBrownCS](https://youtube.com/playlist?list=PL04uZ7242_M5KDdD9XnuWb2MNYsM57HpU&si=15e8xPqMTasTXBO3) Paper 1 (FREE)

[Smart Revise](https://www.smartrevise.online/) Active recall platform (PAID)

Programming:

[MrBrownCS](https://youtube.com/playlist?list=PL04uZ7242_M7ZaSg6cCku-8W-IoLlQs6R&si=6GgRRpKpyf4ZF0bv) Paper 2 (FREE)

[Python Docs](https://docs.python.org/3/) Detailed language documentation (FREE)

[FreeCodeCamp](https://www.freecodecamp.org/) Guided programming (FREE)

[Tech With Tim](https://youtu.be/VchuKL44s6E?si=QhwMl8TLaZO0hKuN) Python couses, [1](https://youtube.com/playlist?list=PLzMcBGfZo4-mFu00qxl0a67RhjjZj3jXm&si=gR7SEjrY3pkAmTlo) [2](https://youtube.com/playlist?list=PLzMcBGfZo4-nhWva-6OVh1yKWHBs4o_tv&si=3v_uN6ztdDLRARZd) [3](https://youtube.com/playlist?list=PLzMcBGfZo4-kwmIcMDdXSuy_wSqtU-xDP&si=aSK3oI8Or_-4EoFA) (FREE)

[Calub Curry](https://youtube.com/playlist?list=PL_c9BZzLwBRLrHc6MntpdrNPKoC2tJr0z&si=KYAOMFwMnMdqqOuL) Detailed python course (FREE)

Exams:

[Past Papers](https://teachcomputerscience.com/past-papers/ocr-gcse/) (FREE)

### Sources and Credit

This document is being maintained by Snazzy Kitten, and has sourced information from:

[Craig'n'Dave](https://youtube.com/playlist?list=PLCiOXwirraUAEhj4TUjMxYm4593B2dUPF&si=yNPTfAsj8xeOcuUv)

[PG Online Textbook](https://www.pgonline.co.uk/resources/computer-science/gcse-ocr/?tab=j277-units)

[BBC Bitesize](https://www.bbc.co.uk/bitesize/examspecs/zmtchbk)

Additional Credits for contributions, and assistance:
- N/A









